package com.nat.domain.models

class Score(val finalScore: String,
            val halfTimeScore: String = "",
            val additionalTimeScore: String = "",
            val penaltyScore: String = ""
)