package com.nat.fifapplication.di.modules.statisctic

import com.nat.domain.business.statistic.StatisticInteractor
import com.nat.fifapplication.di.scope.PerFragment
import com.nat.fifapplication.model.mapper.StatisticViewMapper
import com.nat.fifapplication.presenter.statistic.StatisticPresenter
import com.nat.fifapplication.utils.ResourcesManager
import dagger.Module
import dagger.Provides

@Module
class StatisticPresentationModule {

    @Provides
    @PerFragment
    fun provideStatisticPresenter(interactor: StatisticInteractor,
                                  mapper: StatisticViewMapper,
                                  resourcesManager: ResourcesManager)
            = StatisticPresenter(interactor, mapper, resourcesManager)

    @Provides
    fun provideStatisticViewMapper(resourcesManager: ResourcesManager)
            = StatisticViewMapper(resourcesManager)
}