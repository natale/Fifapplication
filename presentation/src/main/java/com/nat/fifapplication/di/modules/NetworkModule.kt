package com.nat.fifapplication.di.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.nat.data.DataConfig
import com.nat.data.DataConfig.DATE_FORMAT
import com.nat.data.api.FootballApi
import com.nat.data.models.Round
import com.nat.data.utils.deserializers.RoundsDeserializer
import com.nat.fifapplication.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideApi(retrofit: Retrofit): FootballApi =
            retrofit.create(FootballApi::class.java)

    @Provides
    @Singleton
    fun provideGson(): Gson =
            GsonBuilder()
                    .registerTypeAdapter(object : TypeToken<List<@JvmSuppressWildcards Round>>() {}.type, RoundsDeserializer(Gson()))
                    .setDateFormat(DATE_FORMAT)
                    .create()

    @Provides
    @Singleton
    fun provideRetrofitClient(client: OkHttpClient, gson: Gson): Retrofit =
            Retrofit.Builder()
                    .baseUrl(DataConfig.GITHUB_ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(client)
                    .build()


    @Provides
    @Singleton
    fun provideClient(interceptor: HttpLoggingInterceptor): OkHttpClient =
            OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .build()

    @Provides
    @Singleton
    fun provideLoggingInterceptor(): HttpLoggingInterceptor =
            HttpLoggingInterceptor { message -> Timber.d(message) }.apply {
                level = if (BuildConfig.DEBUG) Level.BODY else Level.NONE
            }
}