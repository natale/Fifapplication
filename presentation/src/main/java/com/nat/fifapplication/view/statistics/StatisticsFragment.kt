package com.nat.fifapplication.view.statistics

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.transition.TransitionInflater
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.nat.fifapplication.R
import com.nat.fifapplication.databinding.FragmentStatisticsBinding
import com.nat.fifapplication.model.MatchUiModel
import com.nat.fifapplication.presenter.statistic.StatisticPresenter
import com.nat.fifapplication.view.recycler.RecyclerViewItem
import com.nat.fifapplication.view.recycler.VerticalSpaceItemDecoration
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject
import javax.inject.Provider

class StatisticsFragment : MvpAppCompatFragment(), StatisticView {

    companion object {
        const val EXTRA_TRANSITION_NAMES = "transition_name"
        const val EXTRA_MATCH = "match"
    }

    @InjectPresenter
    lateinit var presenter: StatisticPresenter

    @Inject
    lateinit var provider: Provider<StatisticPresenter>

    lateinit var binding: FragmentStatisticsBinding

    private val detailsAdapter: DetailsAdapter = DetailsAdapter()

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postponeEnterTransition()
        sharedElementEnterTransition = TransitionInflater.from(context)
                .inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_statistics, container, false)
        with (binding.recyclerView) {
            adapter = detailsAdapter
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(VerticalSpaceItemDecoration(resources.getDimensionPixelOffset(R.dimen.recycler_item_offset)))
        }

        return binding.root
    }

    @ProvidePresenter
    fun providePresenter(): StatisticPresenter {
        val presenter = provider.get()
        arguments?.let {
            val matchUiModel: MatchUiModel = it.getParcelable(EXTRA_MATCH)
            presenter.match = matchUiModel
        }
        return presenter
    }

    override fun showMatchInfo(matchUiModel: MatchUiModel) {
        binding.toolbar.match = matchUiModel
        binding.match = matchUiModel
        arguments?.let {
            val transitionNames = it.getStringArray(EXTRA_TRANSITION_NAMES)
            binding.toolbar.tvHomeTeam.transitionName = transitionNames[0]
            binding.toolbar.tvGuestTeam.transitionName = transitionNames[1]
        }

        startPostponedEnterTransition()
    }

    override fun showDetails(details: List<RecyclerViewItem>) {
        detailsAdapter.addItems(details)
    }

    override fun showError(message: String) {
        Snackbar.make(binding.coordinatorLayout, message, Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(R.string.error_close), { _ -> goBack() })
                .show()
    }

    private fun goBack() {
        val navController: NavController = Navigation.findNavController(requireActivity(), R.id.navHostFragment)
        navController.popBackStack()
    }
}