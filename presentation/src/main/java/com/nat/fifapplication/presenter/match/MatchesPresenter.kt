package com.nat.fifapplication.presenter.match

import com.arellomobile.mvp.InjectViewState
import com.nat.domain.business.matches.MatchesInteractor
import com.nat.fifapplication.R
import com.nat.fifapplication.model.mapper.MatchViewMapper
import com.nat.fifapplication.presenter.base.BasePresenter
import com.nat.fifapplication.utils.ResourcesManager
import com.nat.fifapplication.view.match.MatchesView
import io.reactivex.android.schedulers.AndroidSchedulers
import timber.log.Timber

@InjectViewState
class MatchesPresenter(private val interactor: MatchesInteractor,
                       private val mapper: MatchViewMapper,
                       private val resourcesManager: ResourcesManager) : BasePresenter<MatchesView>() {

    override fun onFirstViewAttach() {
        loadMatchesList()
    }

    private fun loadMatchesList() {
        val disposable = interactor.getAllMatches()
                .map { mapper.convert(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { _ -> viewState.showLoading() }
                .doAfterTerminate { viewState.hideLoading() }
                .doOnError{ Timber.e(it) }
                .subscribe(
                        { matches -> viewState.showContent(matches) },
                        { _ -> viewState.showError(resourcesManager.getString(R.string.error_common_text)) }
                )
        addDisposable(disposable)
    }

    fun onRetry() {
        loadMatchesList()
    }
}