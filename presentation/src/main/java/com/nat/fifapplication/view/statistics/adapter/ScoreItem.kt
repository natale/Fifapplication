package com.nat.fifapplication.view.statistics.adapter

import android.support.v7.widget.RecyclerView
import com.nat.fifapplication.R
import com.nat.fifapplication.databinding.ItemListScoreBinding
import com.nat.fifapplication.model.ScoreUiModel
import com.nat.fifapplication.view.recycler.BindableViewHolder
import com.nat.fifapplication.view.recycler.RecyclerViewItem

class ScoreItem(val scoreUiModel: ScoreUiModel) : RecyclerViewItem {
    @Suppress("UNCHECKED_CAST")
    override fun bind(holder: RecyclerView.ViewHolder) {
        val viewHolder = holder as BindableViewHolder<ItemListScoreBinding>
        viewHolder.binding.score = scoreUiModel
    }

    override fun getLayout(): Int = R.layout.item_list_score
}