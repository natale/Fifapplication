package com.nat.domain.models

class Stadium(val name: String, val city: String)