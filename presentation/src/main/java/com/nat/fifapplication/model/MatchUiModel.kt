package com.nat.fifapplication.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MatchUiModel(val id: Long,
                        val homeTeam: String,
                        val guestTeam: String,
                        val homeTeamFlag: String,
                        val guestTeamFlag: String,
                        val score: String,
                        val date: String) : Parcelable
