package com.nat.fifapplication.view.recycler

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class VerticalSpaceItemDecoration(private val verticalSpaceHeight: Int): RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                       state: RecyclerView.State) {
        val itemPosition = parent.getChildAdapterPosition(view)
        val itemCount = state.itemCount

        outRect.top = verticalSpaceHeight
        if (itemCount > 0 && itemPosition == itemCount - 1) {
            outRect.bottom = verticalSpaceHeight
        }
    }
}