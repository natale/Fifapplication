package com.nat.domain.models

data class Team(val flagUrl: String,
                val code: String,
                val name: String)