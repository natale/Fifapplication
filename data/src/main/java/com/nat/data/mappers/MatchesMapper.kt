package com.nat.data.mappers

import com.nat.data.models.MatchDto
import com.nat.domain.models.Match

class MatchesMapper(private val teamMapper: TeamMapper,
                    private val scoreMapper: ScoreMapper) {

    fun convert(dto: MatchDto): Match {
        val homeTeam = teamMapper.getTeam(dto.homeTeam)
        val guestTeam = teamMapper.getTeam(dto.guestTeam)

        val match = dto.match
        return Match(match.id,
                scoreMapper.getScore(match),
                home = homeTeam,
                guest = guestTeam,
                date = match.date)
    }
}