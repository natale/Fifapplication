package com.nat.fifapplication.view

import android.os.Bundle
import com.nat.fifapplication.R
import com.nat.fifapplication.view.base.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
