package com.nat.fifapplication.model
data class GoalUiModel(val author: String,
                       val minute: String,
                       val homeTeam: Boolean,
                       val penalty: Boolean,
                       val ownGoal: Boolean): UiModel