package com.nat.fifapplication.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import com.nat.data.db.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PersistenceModule {
    companion object {
        const val DATABASE_NAME = "fifapplication_database"
    }

    @Provides
    @Singleton
    fun provideAppDatabase(context: Context): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build()

    @Provides
    @Singleton
    fun provideTeamDao(appDatabase: AppDatabase) = appDatabase.teamDao()

    @Provides
    @Singleton
    fun provideMatchDao(appDatabase: AppDatabase) = appDatabase.matchDao()

    @Provides
    @Singleton
    fun provideGoalDao(appDatabase: AppDatabase) = appDatabase.goalDao()
}