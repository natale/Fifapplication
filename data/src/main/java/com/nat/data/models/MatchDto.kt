package com.nat.data.models

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.Relation

class MatchDto {
    @Embedded
    lateinit var match: MatchEntity

    @Relation(
            parentColumn = "id",
            entityColumn = "matchId",
            entity = GoalDto::class)
    lateinit var goals: List<GoalDto>

    @Ignore
    lateinit var homeTeam: TeamDto

    @Ignore
    lateinit var guestTeam: TeamDto
}