package com.nat.domain.models

data class Goal(val author: String,
           val minute: Int,
           val offset: Int,
           val score: String,
           val homeTeam: Boolean,
           val penalty: Boolean,
           val ownGoal: Boolean)