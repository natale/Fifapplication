package com.nat.data

object DataConfig {
    const val GITHUB_ENDPOINT = "https://raw.githubusercontent.com/openfootball/world-cup.json/master/"
    const val FLAG_URL_PLACEHOLDER = "https://restcountries.eu/data/%s.svg"
    const val DATE_FORMAT = "yyyy-MM-dd"
}