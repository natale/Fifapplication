package com.nat.data.models

import com.google.gson.annotations.SerializedName

class Round(@SerializedName("matches") val matches: List<MatchResponse>)