package com.nat.domain.business.matches

import com.nat.domain.models.Match
import com.nat.domain.repositories.MatchesRepository
import io.reactivex.Single

class MatchesInteractorImpl (private val repository: MatchesRepository) : MatchesInteractor {
    override fun getAllMatches(): Single<List<Match>> {
        return repository.getMatches()
    }
}