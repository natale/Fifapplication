package com.nat.fifapplication.presenter.statistic

import com.arellomobile.mvp.InjectViewState
import com.nat.domain.business.statistic.StatisticInteractor
import com.nat.fifapplication.R
import com.nat.fifapplication.model.MatchUiModel
import com.nat.fifapplication.model.mapper.StatisticViewMapper
import com.nat.fifapplication.presenter.base.BasePresenter
import com.nat.fifapplication.utils.ResourcesManager
import com.nat.fifapplication.view.statistics.StatisticView
import io.reactivex.android.schedulers.AndroidSchedulers
import timber.log.Timber

@InjectViewState
class StatisticPresenter(private val interactor: StatisticInteractor,
                         private val mapper: StatisticViewMapper,
                         private val resourcesManager: ResourcesManager)
    : BasePresenter<StatisticView>() {

    lateinit var match: MatchUiModel

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showMatchInfo(match)
        addDisposable(interactor.getMatchStatistic(match.id)
                .map { mapper.apply(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError { Timber.e(it) }
                .subscribe({ items -> viewState.showDetails(items) },
                        { _ -> resourcesManager.getString(R.string.error_common_text)}))
    }
}