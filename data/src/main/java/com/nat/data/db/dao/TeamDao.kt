package com.nat.data.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.nat.data.models.TeamDto
import io.reactivex.Single


@Dao
interface TeamDao {
    @Query("SELECT * FROM teamdto WHERE code = :code")
    fun getByName(code: String): Single<TeamDto>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(teamDto: TeamDto): Long
}