package com.nat.fifapplication.di.modules.statisctic

import com.nat.data.mappers.ScoreMapper
import com.nat.data.mappers.StatisticMapper
import com.nat.data.mappers.TeamMapper
import com.nat.data.repositories.LocalStatisticRepository
import com.nat.data.repositories.match.MatchesCache
import com.nat.domain.business.statistic.StatisticInteractor
import com.nat.domain.business.statistic.StatisticInteractorImpl
import com.nat.domain.repositories.StatisticRepository
import com.nat.fifapplication.di.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class StatisticModule {
    @Provides
    @PerFragment
    fun provideStatisticInteractor(repository: StatisticRepository): StatisticInteractor =
            StatisticInteractorImpl(repository)

    @Provides
    @PerFragment
    fun provideStatisticRepository(matchesCache: MatchesCache,
                                   mapper: StatisticMapper): StatisticRepository =
            LocalStatisticRepository(matchesCache, mapper)

    @Provides
    @PerFragment
    fun provideStatisticMapper(teamMapper: TeamMapper,
                               scoreMapper: ScoreMapper) =
            StatisticMapper(teamMapper, scoreMapper)
}