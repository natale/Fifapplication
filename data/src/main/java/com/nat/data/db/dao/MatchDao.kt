package com.nat.data.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.nat.data.models.MatchDto
import com.nat.data.models.MatchEntity
import io.reactivex.Single

@Dao
interface MatchDao {
    @Transaction
    @Query("SELECT * FROM MatchEntity")
    fun getMatches(): Single<List<MatchDto>>

    @Transaction
    @Query("SELECT * FROM MatchEntity WHERE MatchEntity.id = :id")
    fun getMatchById(id: Long): Single<MatchDto>

    @Insert
    fun insert(match: MatchEntity): Long
}