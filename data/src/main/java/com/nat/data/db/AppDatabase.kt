package com.nat.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.nat.data.db.dao.GoalDao
import com.nat.data.db.dao.MatchDao
import com.nat.data.db.dao.TeamDao
import com.nat.data.models.GoalDto
import com.nat.data.models.MatchEntity
import com.nat.data.models.TeamDto


@Database(entities = [TeamDto::class, GoalDto::class, MatchEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun teamDao(): TeamDao
    abstract fun matchDao(): MatchDao
    abstract fun goalDao(): GoalDao
}