package com.nat.data.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import com.nat.data.db.MatchConverters
import java.util.*

@Entity
@TypeConverters(MatchConverters::class)
class MatchEntity {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    lateinit var homeTeamCode: String

    lateinit var guestTeamCode: String

    lateinit var date: Date

    lateinit var time: String

    var homeTeamScore: Int = 0

    var guestTeamScore: Int = 0

    var halfTimeHomeTeamScore: Int = 0

    var halfTimeGuestTeamScore: Int = 0

    var additionalTimeHomeTeamScore: Int? = null

    var additionalGuestTeamScore: Int? = null

    var penaltyHomeTeamScore: Int? = null

    var penaltyGuestTeamScore: Int? = null

    lateinit var city: String

    lateinit var stadium: StadiumDto
}