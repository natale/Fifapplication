package com.nat.fifapplication.view.recycler

import android.support.v7.widget.RecyclerView

interface RecyclerViewItem {
    fun bind(holder: RecyclerView.ViewHolder)
    fun getLayout(): Int
}