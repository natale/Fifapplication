package com.nat.data.repositories.match

import com.nat.data.models.MatchDto
import com.nat.data.models.MatchResponse
import io.reactivex.Flowable
import io.reactivex.Single

interface MatchesCache {
    fun getMatches(): Flowable<MatchDto>
    fun saveMatch(match: MatchResponse): Single<MatchDto>
    fun getMatchById(id: Long) : Single<MatchDto>
}