package com.nat.data.mappers

import com.nat.data.models.GoalDto
import com.nat.domain.models.Goal

class GoalMapper {

    fun convertGoal(from: GoalDto) =
        Goal(from.name,
                from.minute,
                from.offset,
                "${from.homeTeamScore} - ${from.guestTeamScore}",
                from.homeTeam,
                penalty = from.penalty,
                ownGoal = from.ownGoal)
}