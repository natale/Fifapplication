package com.nat.fifapplication.model

class ScoreUiModel(val halfTimeScore: String,
                   val additionalTimeScore: String,
                   val penaltyScore: String) : UiModel