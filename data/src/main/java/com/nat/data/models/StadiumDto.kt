package com.nat.data.models

import com.google.gson.annotations.SerializedName

class StadiumDto(
        @SerializedName("name") val name: String
)