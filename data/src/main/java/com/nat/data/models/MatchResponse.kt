package com.nat.data.models

import com.google.gson.annotations.SerializedName
import java.util.*

class MatchResponse {
    @SerializedName("team1")
    lateinit var homeTeam: TeamDto

    @SerializedName("team2")
    lateinit var guestTeam: TeamDto

    @SerializedName("date")
    lateinit var date: Date

    @SerializedName("time")
    lateinit var time: String

    @SerializedName("score1")
    var homeTeamScore: Int = 0

    @SerializedName("score2")
    var guestTeamScore: Int = 0

    @SerializedName("score1i")
    var halfTimeHomeTeamScore: Int = 0

    @SerializedName("score2i")
    var halfTimeGuestTeamScore: Int = 0

    @SerializedName("score1et")
    var additionalTimeHomeTeamScore: Int? = null

    @SerializedName("score2et")
    var additionalGuestTeamScore: Int? = null

    @SerializedName("score1p")
    var penaltyHomeTeamScore: Int? = null

    @SerializedName("score2p")
    var penaltyGuestTeamScore: Int? = null

    @SerializedName("goals1")
    var homeTeamGoals: List<GoalDto> = listOf()

    @SerializedName("goals2")
    var guestTeamGoals: List<GoalDto> = listOf()

    @SerializedName("city")
    lateinit var city: String

    @SerializedName("stadium")
    lateinit var stadium: StadiumDto
}