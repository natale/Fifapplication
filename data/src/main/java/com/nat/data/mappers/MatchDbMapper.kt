package com.nat.data.mappers

import com.nat.data.models.MatchDto
import com.nat.data.models.MatchEntity
import com.nat.data.models.MatchResponse

class MatchDbMapper {

    fun convert(match: MatchResponse): MatchEntity {

        val matchTable = MatchEntity()
        matchTable.date = match.date
        matchTable.time = match.time
        matchTable.homeTeamScore = match.homeTeamScore
        matchTable.guestTeamScore = match.guestTeamScore
        matchTable.halfTimeHomeTeamScore = match.halfTimeHomeTeamScore
        matchTable.halfTimeGuestTeamScore = match.halfTimeGuestTeamScore

        matchTable.additionalTimeHomeTeamScore = match.additionalTimeHomeTeamScore
        matchTable.additionalGuestTeamScore = match.additionalGuestTeamScore

        matchTable.penaltyHomeTeamScore = match.penaltyHomeTeamScore
        matchTable.penaltyGuestTeamScore = match.penaltyGuestTeamScore

        matchTable.city = match.city
        matchTable.stadium = match.stadium
        matchTable.homeTeamCode = match.homeTeam.code
        matchTable.guestTeamCode = match.guestTeam.code

        return matchTable
    }

    fun convert(match: MatchEntity, response: MatchResponse): MatchDto {
        val matchDto = MatchDto()
        matchDto.match = match
        matchDto.homeTeam = response.homeTeam
        matchDto.guestTeam = response.guestTeam
        return matchDto
    }
}