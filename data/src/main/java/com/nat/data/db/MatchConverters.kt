package com.nat.data.db

import android.arch.persistence.room.TypeConverter
import com.nat.data.models.StadiumDto
import java.util.*


class MatchConverters {

    @TypeConverter
    fun fromStadium(stadiumDto: StadiumDto) = stadiumDto.name

    @TypeConverter
    fun toStadium(name: String) = StadiumDto(name)

    @TypeConverter
    fun dateToTimestamp(date: Date) = date.time

    @TypeConverter
    fun timestampToDate(timestamp: Long) = Date(timestamp)

}