package com.nat.domain.business.matches

import com.nat.domain.models.Match
import io.reactivex.Single

interface MatchesInteractor {
    fun getAllMatches() : Single<List<Match>>
}