package com.nat.domain.business.statistic

import com.nat.domain.models.Statistic
import io.reactivex.Single

interface StatisticInteractor {
    fun getMatchStatistic(id: Long): Single<Statistic>
}