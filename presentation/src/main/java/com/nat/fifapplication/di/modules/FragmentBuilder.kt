package com.nat.fifapplication.di.modules

import com.nat.fifapplication.di.modules.match.MatchesModule
import com.nat.fifapplication.di.modules.match.MatchesPresentationModule
import com.nat.fifapplication.di.modules.statisctic.StatisticModule
import com.nat.fifapplication.di.modules.statisctic.StatisticPresentationModule
import com.nat.fifapplication.di.scope.PerFragment
import com.nat.fifapplication.view.match.MatchesFragment
import com.nat.fifapplication.view.statistics.StatisticsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {
    @PerFragment
    @ContributesAndroidInjector(modules = arrayOf(MatchesPresentationModule::class, MatchesModule::class))
    abstract fun provideMatchesFragment(): MatchesFragment

    @PerFragment
    @ContributesAndroidInjector(modules = arrayOf(StatisticPresentationModule::class, StatisticModule::class))
    abstract fun provideStatisticFragment(): StatisticsFragment
}