package com.nat.domain.repositories

import com.nat.domain.models.Statistic
import io.reactivex.Single

interface StatisticRepository {
    fun getMatchStatistic(id: Long): Single<Statistic>
}