package com.nat.data.repositories.team

import com.nat.data.models.TeamDto
import io.reactivex.Single

interface TeamRepository {
    fun insert(team: TeamDto): Single<Long>
    fun get(code: String): Single<TeamDto>
}