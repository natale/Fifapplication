package com.nat.fifapplication.view.statistics

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.nat.fifapplication.view.recycler.BindableViewHolder
import com.nat.fifapplication.view.recycler.RecyclerViewItem

class DetailsAdapter(private val items: MutableList<RecyclerViewItem> = mutableListOf()) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, layoutResId: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding : ViewDataBinding = DataBindingUtil.inflate(inflater, layoutResId, parent, false)
        return BindableViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val contentItem = items[position]
        contentItem.bind(holder)
    }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int) = items[position].getLayout()

    fun addItems(list: List<RecyclerViewItem>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }
}