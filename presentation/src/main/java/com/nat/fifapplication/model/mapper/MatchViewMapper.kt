package com.nat.fifapplication.model.mapper

import com.nat.domain.models.Match
import com.nat.domain.models.Score
import com.nat.fifapplication.model.MatchUiModel
import com.nat.fifapplication.utils.DateFormatter

class MatchViewMapper(private val dateFormatter: DateFormatter) {

    fun convert(matches: List<Match>): List<MatchUiModel> {
        return matches.map { convert(it) }
    }

    private fun convert(from: Match): MatchUiModel {
        return MatchUiModel(from.id,
                from.home.code, from.guest.code,
                from.home.flagUrl, from.guest.flagUrl,
                getScoreToDisplay(from.score), dateFormatter.format(from.date))
    }

    private fun getScoreToDisplay(score: Score): String =
            if (score.additionalTimeScore.isNotEmpty()) score.additionalTimeScore
            else score.finalScore
}