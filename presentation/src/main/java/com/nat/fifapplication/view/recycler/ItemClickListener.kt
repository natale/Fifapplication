package com.nat.fifapplication.view.recycler

import android.view.View

interface ItemClickListener<T> {
    fun onItemClick(item: T, views: List<View>)
}