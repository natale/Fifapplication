package com.nat.fifapplication.model.mapper

import com.nat.domain.models.Goal
import com.nat.domain.models.Statistic
import com.nat.fifapplication.R
import com.nat.fifapplication.model.GoalUiModel
import com.nat.fifapplication.model.ScoreUiModel
import com.nat.fifapplication.model.StadiumUiModel
import com.nat.fifapplication.utils.ResourcesManager
import com.nat.fifapplication.view.recycler.RecyclerViewItem
import com.nat.fifapplication.view.statistics.adapter.GoalItem
import com.nat.fifapplication.view.statistics.adapter.ScoreItem
import com.nat.fifapplication.view.statistics.adapter.StadiumItem
import io.reactivex.functions.Function

class StatisticViewMapper(private val resourcesManager: ResourcesManager) : Function<Statistic, List<RecyclerViewItem>> {

    companion object {
        const val POSITION_GOALS = 1
        const val NO_OFFSET = 0
    }

    override fun apply(statistic: Statistic): List<RecyclerViewItem> {
        val score = convertScore(statistic)
        val goals = convertGoals(statistic)
        val stadium = convertStadium(statistic)

        val result: MutableList<RecyclerViewItem> = mutableListOf(score, stadium)
        result.addAll(POSITION_GOALS, goals)
        return result
    }

    private fun convertScore(statistic: Statistic): RecyclerViewItem {
        val score = statistic.score
        val uiModel = ScoreUiModel(score.halfTimeScore, score.additionalTimeScore, score.penaltyScore)
        return ScoreItem(uiModel)
    }

    private fun convertGoals(statistic: Statistic): List<RecyclerViewItem> {
        return statistic.goals.map { createGoal(it) }
                .map { GoalItem(it) }
    }

    private fun convertStadium(statistic: Statistic): RecyclerViewItem {
        val stadium = StadiumUiModel(statistic.stadium.name, statistic.stadium.city)
        return StadiumItem(stadium)
    }

    private fun createGoal(goal: Goal): GoalUiModel {
        val time: String = if (goal.offset == NO_OFFSET) goal.minute.toString()
            else "${goal.minute} + ${goal.offset}"

        val formattedTime = resourcesManager.getString(R.string.minute_pattern, time)
        return GoalUiModel(goal.author, formattedTime, goal.homeTeam, goal.penalty, goal.ownGoal)
    }
}