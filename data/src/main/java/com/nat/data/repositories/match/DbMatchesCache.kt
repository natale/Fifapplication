package com.nat.data.repositories.match

import com.nat.data.db.dao.GoalDao
import com.nat.data.db.dao.MatchDao
import com.nat.data.mappers.MatchDbMapper
import com.nat.data.models.MatchDto
import com.nat.data.models.MatchEntity
import com.nat.data.models.MatchResponse
import com.nat.data.models.TeamDto
import com.nat.data.repositories.team.TeamRepository
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction

class DbMatchesCache(private val teamRepository: TeamRepository,
                     private val matchDao: MatchDao,
                     private val goalsDao: GoalDao,
                     private val matchDbMapper: MatchDbMapper) : MatchesCache {

    override fun getMatches(): Flowable<MatchDto> =
            matchDao.getMatches()
                    .filter { it.isNotEmpty() }
                    .toSingle()
                    .flatMapPublisher { Flowable.fromIterable(it) }
                    .flatMapSingle { getTeams(it) }

    override fun saveMatch(match: MatchResponse): Single<MatchDto> =
            Single.defer { Single.just(matchDbMapper.convert(match)) }
                    .doOnSuccess { it.id = matchDao.insert(it) }
                    .flatMap { saveTeams(it, match) }
                    .doOnSuccess { insertGoals(it.id, match) }
                    .map { matchDbMapper.convert(it, match) }

    override fun getMatchById(id: Long): Single<MatchDto> {
        return matchDao.getMatchById(id)
                .flatMap { getTeams(it) }
    }

    /**
     * Workaround to deal with //https://issuetracker.google.com/issues/62848977
     */
    private fun insertGoals(matchId: Long, match: MatchResponse) {
        for (goal in match.homeTeamGoals) {
            goal.matchId = matchId
            goal.homeTeam = true
        }

        for (goal in match.guestTeamGoals) {
            goal.matchId = matchId
        }

        goalsDao.insert(match.homeTeamGoals)
        goalsDao.insert(match.guestTeamGoals)
    }

    private fun saveTeams(dto: MatchEntity, match: MatchResponse): Single<MatchEntity> =
            Single.zip(
                    insertTeam(dto.id, match.homeTeam),
                    insertTeam(dto.id, match.guestTeam),
                    BiFunction { _, _ -> dto })

    private fun insertTeam(matchId: Long, teamDto: TeamDto): Single<Long> =
            Single.just(teamDto)
                    .doOnSuccess { it.matchId = matchId }
                    .flatMap { teamRepository.insert(it) }

    private fun getTeams(match: MatchDto) : Single<MatchDto> =
        Single.zip(
                teamRepository.get(match.match.homeTeamCode),
                teamRepository.get(match.match.guestTeamCode),
                BiFunction { homeTeam, guestTeam ->
                    match.homeTeam = homeTeam
                    match.guestTeam = guestTeam
                    match
                })
}