package com.nat.data.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import com.nat.data.models.GoalDto

@Dao
interface GoalDao {
    @Insert
    fun insert(goals: List<GoalDto>)
}