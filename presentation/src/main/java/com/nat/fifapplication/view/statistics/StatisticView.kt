package com.nat.fifapplication.view.statistics

import com.arellomobile.mvp.MvpView
import com.nat.fifapplication.model.MatchUiModel
import com.nat.fifapplication.view.recycler.RecyclerViewItem

interface StatisticView: MvpView {
    fun showMatchInfo(matchUiModel: MatchUiModel)
    fun showDetails(details: List<RecyclerViewItem>)
    fun showError(message: String)
}