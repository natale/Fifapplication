package com.nat.data.utils.deserializers

import com.google.gson.*
import com.nat.data.models.Round
import java.lang.reflect.Type

class RoundsDeserializer(private val gson: Gson): JsonDeserializer<List<Round>> {
    companion object {
        const val FIELD_NAME = "rounds"
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): List<Round>? {
        val jsonObject : JsonObject = json as JsonObject
        val content = jsonObject.get(FIELD_NAME).asJsonArray
        return gson.fromJson(content, typeOfT)
    }
}