package com.nat.fifapplication.view.match

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.github.lion4ik.arch.sharedelements.HasSharedElements
import com.nat.fifapplication.R
import com.nat.fifapplication.databinding.FragmentMatchesBinding
import com.nat.fifapplication.model.MatchUiModel
import com.nat.fifapplication.presenter.match.MatchesPresenter
import com.nat.fifapplication.view.recycler.ItemClickListener
import com.nat.fifapplication.view.statistics.StatisticsFragment
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject
import javax.inject.Provider

class MatchesFragment : MvpAppCompatFragment(), MatchesView, HasSharedElements {

    @InjectPresenter
    lateinit var presenter: MatchesPresenter

    @Inject
    lateinit var provider: Provider<MatchesPresenter>

    private val matchesAdapter: MatchesAdapter = MatchesAdapter()
    private lateinit var binding : FragmentMatchesBinding

    //todo remove when issue is resolved https://issuetracker.google.com/issues/79665225
    private var sharedElements: MutableMap<String, View> = mutableMapOf()

    @ProvidePresenter
    fun providePresenter(): MatchesPresenter = provider.get()

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_matches, container, false)
        setUpList()
        return binding.root
    }

    override fun showContent(content: List<MatchUiModel>) {
        matchesAdapter.setItems(content)
    }

    override fun showLoading() {
        binding.progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        binding.progressBar.visibility = View.GONE
    }

    override fun showError(error: String) {
        Snackbar.make(binding.coordinatorLayout, error, Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(R.string.error_retry), { _ -> presenter.onRetry() })
                .show()
    }

    override fun getSharedElements(): Map<String, View> {
        return sharedElements
    }

    override fun hasReorderingAllowed(): Boolean = false

    private fun setUpList() {
        with (binding.recyclerView) {
            adapter = matchesAdapter
            layoutManager = LinearLayoutManager(requireContext())

            val divider = ContextCompat.getDrawable(context, R.drawable.shape_line)
            divider?.let {
                val decoration = SimpleDividerItemDecoration(divider)
                addItemDecoration(decoration)
            }
        }

        matchesAdapter.setItemClickListener(object: ItemClickListener<MatchUiModel> {
            override fun onItemClick(item: MatchUiModel, views: List<View>) {
                sharedElements.clear()
                views.map { ViewCompat.getTransitionName(it) to it }.toMap(sharedElements)

                val transitionNames = views.map { ViewCompat.getTransitionName(it) }.toTypedArray()
                navigateToDetails(item, transitionNames)
            }
        })
    }

    private fun navigateToDetails(match: MatchUiModel, transitionNames: Array<String>) {
        val navController = Navigation.findNavController(requireActivity(), R.id.navHostFragment)
        navController.navigate(R.id.detailsFragment, Bundle().apply {
            putParcelable(StatisticsFragment.EXTRA_MATCH, match)
            putStringArray(StatisticsFragment.EXTRA_TRANSITION_NAMES, transitionNames)
        })
    }
}