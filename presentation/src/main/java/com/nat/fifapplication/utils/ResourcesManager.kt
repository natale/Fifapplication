package com.nat.fifapplication.utils

import android.content.Context
import android.support.annotation.StringRes

class ResourcesManager(private val context: Context) {
    fun getString(resId: Int): String = context.getString(resId)

    fun getString(@StringRes resId: Int, vararg formatArgs: Any): String = context.getString(resId, *formatArgs)
}