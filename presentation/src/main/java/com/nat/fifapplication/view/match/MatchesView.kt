package com.nat.fifapplication.view.match

import com.nat.fifapplication.model.MatchUiModel
import com.nat.fifapplication.view.base.LceView

interface MatchesView : LceView<List<MatchUiModel>>