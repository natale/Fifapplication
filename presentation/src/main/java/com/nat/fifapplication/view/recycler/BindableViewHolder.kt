package com.nat.fifapplication.view.recycler

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView

class BindableViewHolder<T : ViewDataBinding>(val binding: T) : RecyclerView.ViewHolder(binding.root)