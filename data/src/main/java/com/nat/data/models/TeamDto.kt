package com.nat.data.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
class TeamDto {
    @PrimaryKey
    @SerializedName("code")
    lateinit var code: String

    @SerializedName("name")
    lateinit var name: String

    var flagUrl: String? = null

    var matchId: Long? = null
}
