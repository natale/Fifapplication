package com.nat.domain.repositories

import com.nat.domain.models.Match
import io.reactivex.Single

interface MatchesRepository {
    fun getMatches(): Single<List<Match>>
    fun getMatchById(id: Long): Single<Match>
}