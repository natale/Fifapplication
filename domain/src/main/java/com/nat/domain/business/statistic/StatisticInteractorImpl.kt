package com.nat.domain.business.statistic

import com.nat.domain.models.Statistic
import com.nat.domain.repositories.StatisticRepository
import io.reactivex.Single

class StatisticInteractorImpl(private val repository: StatisticRepository): StatisticInteractor {
    override fun getMatchStatistic(id: Long): Single<Statistic> {
        return repository.getMatchStatistic(id)
    }
}