package com.nat.fifapplication.di.modules

import com.nat.data.db.dao.GoalDao
import com.nat.data.db.dao.MatchDao
import com.nat.data.db.dao.TeamDao
import com.nat.data.mappers.MatchDbMapper
import com.nat.data.mappers.ScoreMapper
import com.nat.data.mappers.TeamMapper
import com.nat.data.repositories.match.DbMatchesCache
import com.nat.data.repositories.match.MatchesCache
import com.nat.data.repositories.team.LocalTeamRepository
import com.nat.data.repositories.team.TeamRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {
    @Provides
    @Singleton
    fun provideTeamRepository(teamDao: TeamDao): TeamRepository = LocalTeamRepository(teamDao)

    @Provides
    @Singleton
    fun provideMatchCache(teamRepository: TeamRepository,
                          matchDao: MatchDao,
                          goalsDao: GoalDao,
                          matchDbMapper: MatchDbMapper): MatchesCache =
            DbMatchesCache(teamRepository, matchDao, goalsDao, matchDbMapper)

    @Provides
    @Singleton
    fun provideTeamMapper() = TeamMapper()

    @Provides
    @Singleton
    fun provideMatchDbMapper() = MatchDbMapper()

    @Provides
    @Singleton
    fun provideScoreMapper() = ScoreMapper()
}