package com.nat.data.mappers

import com.nat.data.Constants
import com.nat.data.models.MatchEntity
import com.nat.domain.models.Score

class ScoreMapper {
    fun getScore(match: MatchEntity): Score {
        return Score(
                createScoreString(match.homeTeamScore, match.guestTeamScore),
                createScoreString(match.halfTimeHomeTeamScore, match.halfTimeGuestTeamScore),
                createScoreString(match.additionalTimeHomeTeamScore, match.additionalGuestTeamScore),
                createScoreString(match.penaltyHomeTeamScore, match.penaltyGuestTeamScore)
        )
    }

    private fun createScoreString(left: Int?, right: Int?): String {
        if (left == null || right == null) {
            return Constants.EMPTY_STRING
        }

        return "$left - $right"
    }
}