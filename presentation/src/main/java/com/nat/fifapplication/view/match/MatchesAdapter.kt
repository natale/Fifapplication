package com.nat.fifapplication.view.match

import android.databinding.DataBindingUtil
import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nat.fifapplication.R
import com.nat.fifapplication.databinding.ItemListMatchBinding
import com.nat.fifapplication.model.MatchUiModel
import com.nat.fifapplication.view.recycler.ItemClickListener

class MatchesAdapter(private val matches: MutableList<MatchUiModel> = mutableListOf()) : RecyclerView.Adapter<MatchesAdapter.ViewHolder>() {

    private var itemClickListener: ItemClickListener<MatchUiModel>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ItemListMatchBinding = DataBindingUtil.inflate(inflater, R.layout.item_list_match, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = matches.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val matchUiModel = matches[position]
        holder.bind(matchUiModel)
        ViewCompat.setTransitionName(holder.binding.tvHomeTeam, matchUiModel.homeTeam + matchUiModel.id)
        ViewCompat.setTransitionName(holder.binding.tvGuestTeam, matchUiModel.guestTeam + matchUiModel.id)
        holder.setOnClickListener(View.OnClickListener {
            itemClickListener?.onItemClick(matchUiModel, listOf(holder.binding.tvHomeTeam, holder.binding.tvGuestTeam))
        })
    }

    fun setItems(items: List<MatchUiModel>) {
        matches.clear()
        matches.addAll(items)
        notifyDataSetChanged()
    }

    fun setItemClickListener(itemClickListener: ItemClickListener<MatchUiModel>) {
        this.itemClickListener = itemClickListener
    }

    class ViewHolder(val binding: ItemListMatchBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(matchUiModel: MatchUiModel) {
            binding.match = matchUiModel
            binding.executePendingBindings()
        }

        fun setOnClickListener(onClickListener: View.OnClickListener) {
            itemView.setOnClickListener(onClickListener)
        }
    }
}