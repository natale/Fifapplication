package com.nat.fifapplication

import android.app.Activity
import android.app.Application
import com.nat.fifapplication.di.components.AppComponent
import com.nat.fifapplication.di.components.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject

class TheApp: Application(), HasActivityInjector {

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
    @Inject
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        initDagger()
        initLogger()
    }

    private fun initDagger() {
        DaggerAppComponent
                .builder()
                .create(this)
                .inject(this)
    }

    private fun initLogger() {
        Timber.plant(Timber.DebugTree())
    }

    override fun activityInjector(): AndroidInjector<Activity>? {
        return activityDispatchingAndroidInjector
    }
}