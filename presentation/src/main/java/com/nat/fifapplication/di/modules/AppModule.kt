package com.nat.fifapplication.di.modules

import android.content.Context
import com.nat.fifapplication.TheApp
import dagger.Binds
import dagger.Module
import javax.inject.Singleton


@Module
abstract class AppModule {
    @Binds
    @Singleton
    abstract fun providesAppContext(theApp: TheApp): Context
}