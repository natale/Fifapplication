package com.nat.data.repositories.team

import com.nat.data.db.dao.TeamDao
import com.nat.data.models.TeamDto
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class LocalTeamRepository(private val teamDao: TeamDao): TeamRepository {

    override fun insert(team: TeamDto): Single<Long> =
            Single.fromCallable { teamDao.insert(team) }
                    .subscribeOn(Schedulers.io())

    override fun get(code: String): Single<TeamDto> =
            teamDao.getByName(code)
}