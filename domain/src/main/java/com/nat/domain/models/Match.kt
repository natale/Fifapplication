package com.nat.domain.models

import java.util.*

class Match(val id: Long,
            val score: Score,
            val home: Team,
            val guest: Team,
            val date: Date)