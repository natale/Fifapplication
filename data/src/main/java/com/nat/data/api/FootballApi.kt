package com.nat.data.api

import com.nat.data.models.Round
import io.reactivex.Single
import retrofit2.http.GET

interface FootballApi {
    @GET("2018/worldcup.json")
    fun getMatches(): Single<List<Round>>
}