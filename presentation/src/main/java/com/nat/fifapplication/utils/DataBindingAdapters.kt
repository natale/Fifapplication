package com.nat.fifapplication.utils

import android.databinding.BindingAdapter
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nat.fifapplication.R

object DataBindingAdapters {
    @JvmStatic
    @BindingAdapter("imageUrl")
    fun loadImage(view: ImageView, url: String) {
        val context = view.context
        GlideApp.with(context)
                .load(url)
                .placeholder(R.drawable.ic_worldwide)
                .error(R.drawable.ic_worldwide)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(view)
    }

    @JvmStatic
    @BindingAdapter("gravity")
    fun setGravity(view: View, gravity: Int) {
        val params = view.layoutParams as FrameLayout.LayoutParams
        params.gravity = gravity
    }
}