package com.nat.fifapplication.utils

import java.text.SimpleDateFormat
import java.util.*


class DateFormatter {

    companion object {
        const val DATE_FORMAT = "dd MMMM yyyy, EEE"
    }

    private val formatter: SimpleDateFormat = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())

    fun format(date: Date) : String = formatter.format(date)
}