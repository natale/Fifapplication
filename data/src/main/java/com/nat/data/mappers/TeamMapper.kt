package com.nat.data.mappers

import com.nat.data.DataConfig.FLAG_URL_PLACEHOLDER
import com.nat.data.models.TeamDto
import com.nat.domain.models.Team

class TeamMapper {
    private val correctionMap = mapOf(
            "ger" to "deu",
            "den" to "dnk",
            "cro" to "hrv",
            "por" to "prt",
            "uru" to "ury",
            "sui" to "che",
            "ksa" to "sau",
            "crc" to "cri"
    )

    private val exceptionsFlagMap = mapOf(
            "eng" to "https://upload.wikimedia.org/wikipedia/commons/b/be/Flag_of_England.svg"
    )

    fun getTeam(from: TeamDto): Team {
        val code = from.code.toLowerCase()
        val correctedCode = correctionMap[code] ?: code
        val flagUrl = exceptionsFlagMap[code] ?: FLAG_URL_PLACEHOLDER.format(correctedCode)

        return Team(flagUrl, from.code, from.name)
    }
}