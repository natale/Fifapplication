package com.nat.fifapplication.di.components

import com.nat.fifapplication.TheApp
import com.nat.fifapplication.di.modules.*
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
        AndroidSupportInjectionModule::class,
        AppModule::class,
        PersistenceModule::class,
        ActivityBuilder::class,
        NetworkModule::class,
        UtilsModule::class,
        DataModule::class)
)
interface AppComponent : AndroidInjector<TheApp> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<TheApp>()
}