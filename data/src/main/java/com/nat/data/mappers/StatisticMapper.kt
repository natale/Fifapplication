package com.nat.data.mappers

import com.nat.data.models.GoalDto
import com.nat.data.models.MatchDto
import com.nat.domain.models.Goal
import com.nat.domain.models.Stadium
import com.nat.domain.models.Statistic

class StatisticMapper(private val teamMapper: TeamMapper,
                      private val scoreMapper: ScoreMapper) {

    fun convert(from: MatchDto): Statistic {
        val homeTeam = teamMapper.getTeam(from.homeTeam)
        val guestTeam = teamMapper.getTeam(from.guestTeam)
        val score = scoreMapper.getScore(from.match)
        val stadium = Stadium(from.match.stadium.name, from.match.city)
        val goals = createGoals(from)
        return Statistic(homeTeam, guestTeam, score, goals, stadium)
    }

    private fun createGoals(match: MatchDto): List<Goal> {
        val goals = match.goals
                .map { convertGoal(it) }
                .toMutableList()
        return goals.sortedWith(compareBy({ it.minute }, { it.offset }))
    }

    private fun convertGoal(from: GoalDto) =
            Goal(from.name,
                    from.minute,
                    from.offset,
                    "${from.homeTeamScore} - ${from.guestTeamScore}",
                    from.homeTeam,
                    penalty = from.penalty,
                    ownGoal = from.ownGoal)
}