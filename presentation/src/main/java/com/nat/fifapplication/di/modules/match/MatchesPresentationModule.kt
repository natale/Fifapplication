package com.nat.fifapplication.di.modules.match

import com.nat.domain.business.matches.MatchesInteractor
import com.nat.fifapplication.di.scope.PerFragment
import com.nat.fifapplication.model.mapper.MatchViewMapper
import com.nat.fifapplication.presenter.match.MatchesPresenter
import com.nat.fifapplication.utils.DateFormatter
import com.nat.fifapplication.utils.ResourcesManager
import dagger.Module
import dagger.Provides

@Module
class MatchesPresentationModule {

    @Provides
    @PerFragment
    fun provideMatchesPresenter(interactor: MatchesInteractor,
                                mapper: MatchViewMapper,
                                resourcesManager: ResourcesManager) : MatchesPresenter =
            MatchesPresenter(interactor, mapper, resourcesManager)

    @Provides
    @PerFragment
    fun provideMatchViewMapper(dateFormatter: DateFormatter) : MatchViewMapper = MatchViewMapper(dateFormatter)

    @Provides
    @PerFragment
    fun provideDateFormatter() : DateFormatter = DateFormatter()
}