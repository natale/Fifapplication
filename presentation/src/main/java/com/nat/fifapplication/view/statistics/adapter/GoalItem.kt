package com.nat.fifapplication.view.statistics.adapter

import android.support.constraint.ConstraintSet
import android.support.v7.widget.RecyclerView
import com.nat.fifapplication.R
import com.nat.fifapplication.databinding.ItemListGoalBinding
import com.nat.fifapplication.model.GoalUiModel
import com.nat.fifapplication.view.recycler.BindableViewHolder
import com.nat.fifapplication.view.recycler.RecyclerViewItem



class GoalItem(private val goal: GoalUiModel): RecyclerViewItem {

    @Suppress("UNCHECKED_CAST")
    override fun bind(holder: RecyclerView.ViewHolder) {
        val viewHolder = holder as BindableViewHolder<ItemListGoalBinding>
        viewHolder.binding.goal = goal
        if (goal.homeTeam) {
            attachImageToLeft(viewHolder.binding)
        } else {
            attachImageToRight(viewHolder.binding)
        }
    }

    override fun getLayout(): Int = R.layout.item_list_goal

    private fun attachImageToLeft(binding: ItemListGoalBinding) {
        val resources = binding.root.context.resources
        val margin = resources.getDimensionPixelOffset(R.dimen.offset_match_details)

        val constraintSet = ConstraintSet()
        constraintSet.clone(binding.clMain)
        constraintSet.connect(R.id.ivIcon, ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, margin)
        constraintSet.connect(R.id.tvMinute, ConstraintSet.START, R.id.ivIcon, ConstraintSet.END, margin)
        constraintSet.applyTo(binding.clMain)
    }

    private fun attachImageToRight(binding: ItemListGoalBinding) {
        val resources = binding.root.context.resources
        val margin = resources.getDimensionPixelOffset(R.dimen.offset_match_details)

        val constraintSet = ConstraintSet()
        constraintSet.clone(binding.clMain)
        constraintSet.connect(R.id.ivIcon, ConstraintSet.START, R.id.barrierRight, ConstraintSet.END, margin)
        constraintSet.connect(R.id.tvMinute, ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, margin)
        constraintSet.applyTo(binding.clMain)
    }
}