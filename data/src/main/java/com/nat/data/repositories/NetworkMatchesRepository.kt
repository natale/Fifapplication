package com.nat.data.repositories

import com.nat.data.api.FootballApi
import com.nat.data.mappers.MatchesMapper
import com.nat.data.models.MatchDto
import com.nat.data.repositories.match.MatchesCache
import com.nat.domain.models.Match
import com.nat.domain.repositories.MatchesRepository
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class NetworkMatchesRepository(private val footballApi: FootballApi,
                               private val matchesMapper: MatchesMapper,
                               private val matchesCache: MatchesCache) : MatchesRepository {

    override fun getMatches(): Single<List<Match>> =
            getCachedMatches()
                    .onErrorResumeNext { _: Throwable -> fetchMatches() }
                    .map { matchesMapper.convert(it) }
                    .toList()
                    .subscribeOn(Schedulers.io())

    override fun getMatchById(id: Long): Single<Match> {
        return matchesCache.getMatchById(id)
                .map { matchesMapper.convert(it) }
                .subscribeOn(Schedulers.io())
    }

    private fun fetchMatches(): Flowable<MatchDto> =
            footballApi.getMatches()
                    .flatMapPublisher { Flowable.fromIterable(it) }
                    .map { it.matches }
                    .flatMap { Flowable.fromIterable(it) }
                    .flatMapSingle { matchesCache.saveMatch(it) }

    private fun getCachedMatches(): Flowable<MatchDto> =
            matchesCache.getMatches()
}