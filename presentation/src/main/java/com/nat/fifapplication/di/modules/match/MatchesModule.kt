package com.nat.fifapplication.di.modules.match

import com.nat.data.api.FootballApi
import com.nat.data.mappers.MatchesMapper
import com.nat.data.mappers.ScoreMapper
import com.nat.data.mappers.TeamMapper
import com.nat.data.repositories.NetworkMatchesRepository
import com.nat.data.repositories.match.MatchesCache
import com.nat.domain.business.matches.MatchesInteractor
import com.nat.domain.business.matches.MatchesInteractorImpl
import com.nat.domain.repositories.MatchesRepository
import com.nat.fifapplication.di.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class MatchesModule {
    @Provides
    @PerFragment
    fun provideMatchesRepository(footballApi: FootballApi,
                                 matchesMapper: MatchesMapper,
                                 cache: MatchesCache): MatchesRepository =
            NetworkMatchesRepository(footballApi, matchesMapper, cache)

    @Provides
    @PerFragment
    fun provideMatchesInteractor(repository: MatchesRepository): MatchesInteractor =
            MatchesInteractorImpl(repository)

    @Provides
    @PerFragment
    fun provideMatchesMapper(teamMapper: TeamMapper,
                             scoreMapper: ScoreMapper) = MatchesMapper(teamMapper, scoreMapper)
}