package com.nat.fifapplication.presenter.base

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import timber.log.Timber

abstract class BasePresenter<V : MvpView> : MvpPresenter<V>() {

    private val onDestroyViewDisposable: CompositeDisposable = CompositeDisposable()

    protected fun handleError(error: Throwable) {
        Timber.e(error)
    }

    protected fun addDisposable(disposable: Disposable) {
        onDestroyViewDisposable.add(disposable)
    }

    protected fun removeDisposable(disposable: Disposable) {
        onDestroyViewDisposable.remove(disposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        onDestroyViewDisposable.clear()
    }
}