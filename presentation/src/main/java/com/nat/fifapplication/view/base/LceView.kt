package com.nat.fifapplication.view.base

import com.arellomobile.mvp.MvpView

interface LceView<M> : MvpView {
    fun showContent(content: M)
    fun showLoading()
    fun hideLoading()
    fun showError(error: String)
}