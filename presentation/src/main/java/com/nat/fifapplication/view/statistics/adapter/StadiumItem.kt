package com.nat.fifapplication.view.statistics.adapter

import android.support.v7.widget.RecyclerView
import com.nat.fifapplication.R
import com.nat.fifapplication.databinding.ItemListStadiumBinding
import com.nat.fifapplication.model.StadiumUiModel
import com.nat.fifapplication.view.recycler.BindableViewHolder
import com.nat.fifapplication.view.recycler.RecyclerViewItem

class StadiumItem(val stadium: StadiumUiModel): RecyclerViewItem {
    @Suppress("UNCHECKED_CAST")
    override fun bind(holder: RecyclerView.ViewHolder) {
        val viewHolder = holder as BindableViewHolder<ItemListStadiumBinding>
        viewHolder.binding.stadium = stadium
    }

    override fun getLayout(): Int = R.layout.item_list_stadium
}