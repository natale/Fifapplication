package com.nat.domain.models

class Statistic(val homeTeam: Team,
                val guestTeam: Team,
                val score: Score,
                val goals: List<Goal>,
                val stadium: Stadium)