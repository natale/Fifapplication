package com.nat.data.repositories

import com.nat.data.mappers.StatisticMapper
import com.nat.data.repositories.match.MatchesCache
import com.nat.domain.models.Statistic
import com.nat.domain.repositories.StatisticRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class LocalStatisticRepository(private val matchesCache: MatchesCache,
                               private val mapper: StatisticMapper): StatisticRepository {

    override fun getMatchStatistic(id: Long): Single<Statistic> {
        return matchesCache.getMatchById(id)
                .map { mapper.convert(it) }
                .subscribeOn(Schedulers.io())
    }
}