package com.nat.data.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
class GoalDto {
    @PrimaryKey(autoGenerate = true)
    var goalId: Long? = null

    var matchId: Long? = null

    @SerializedName("name")
    lateinit var name: String

    @SerializedName("minute")
    var minute: Int = 0

    @SerializedName("offset")
    var offset: Int = 0

    @SerializedName("score1")
    var homeTeamScore: Int = 0

    @SerializedName("score2")
    var guestTeamScore: Int = 0

    @SerializedName("owngoal")
    var ownGoal: Boolean = false

    @SerializedName("penalty")
    var penalty: Boolean = false

    var homeTeam: Boolean = false
}