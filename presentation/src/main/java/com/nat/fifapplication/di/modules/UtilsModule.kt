package com.nat.fifapplication.di.modules

import android.content.Context
import com.nat.fifapplication.utils.ResourcesManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UtilsModule {
    @Provides
    @Singleton
    fun provideResourceManager(context: Context) : ResourcesManager = ResourcesManager(context)
}